package ru.polydevochki.startapp

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import org.junit.Test

import org.junit.Assert.*
import ru.polydevochki.startapp.data.Condition
import ru.polydevochki.startapp.data.HometaskData
import ru.polydevochki.startapp.utils.Utils
import java.io.FileReader
import java.time.LocalDate
import java.util.*

/**
 * Example local unit lvl1, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest{
    @Test
    fun CalendartoString(){
        var gson = Gson()
        var mCard = HometaskData(1,1,"puck", Calendar.getInstance(),"file:///android_asset/articles/hometask/lvl1/1.1Vybor_nishi.html",Condition.IN_WORK)
        var jsonString = gson.toJson(mCard)
        print(jsonString)
    }

}

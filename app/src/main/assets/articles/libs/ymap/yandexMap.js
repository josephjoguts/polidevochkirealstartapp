ymaps.ready(init);
var adress_needed = [47.22994157427262,39.75063999999998];

function init() {
    var myMap = new ymaps.Map('map', {
        center:[47.22994157427262,39.75063999999998],
        zoom: 16,
        controls: []
    });


    ymaps.geocode(adress_needed, {
        results: 1
    }).then(function (res) {
        myMap.controls.add('zoomControl', { left: 5, top: 5 });
        var firstGeoObject = res.geoObjects.get(0),
            coords = firstGeoObject.geometry.getCoordinates(),
            bounds = firstGeoObject.properties.get('boundedBy');

        myMap.setBounds(bounds, {
            checkZoomRange: true
        });

        var myPlacemark = new ymaps.Placemark(coords, {
            balloonContent: 'Мы здесь, друг!',
            hintContent: ''
        }, {
            iconLayout: 'default#image',
            iconImageHref: 'images/icon-marker.png',
            iconImageSize: [47, 60],
            iconImageOffset: [0, -50]
        });           

        

        myMap.geoObjects.add(myPlacemark);

        //myPlacemark.balloon.open();
    });


}
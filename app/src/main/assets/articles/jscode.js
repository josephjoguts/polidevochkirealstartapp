

function showToast(t)

{
app.showtoast(t);
return false;
}



function parse_query_string(query) {
  var vars = query.split("&");
  var query_string = {};
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    var key = decodeURIComponent(pair[0]);
    var value = decodeURIComponent(pair[1]);
    // If first entry with this name
    if (typeof query_string[key] === "undefined") {
      query_string[key] = decodeURIComponent(value);
      // If second entry with this name
    } else if (typeof query_string[key] === "string") {
      var arr = [query_string[key], decodeURIComponent(value)];
      query_string[key] = arr;
      // If third or later entry with this name
    } else {
      query_string[key].push(decodeURIComponent(value));
    }
  }
  return query_string;
}
$(document).ready(function(){

var query = window.location.search.substring(1);
var parsed_qs = parse_query_string(query);

    window.scrollTo({
      top: query,
      behavior: 'auto'
    });
});




window.onscroll = function(ev) {

    $(".debug").html(window.innerHeight + "; " + window.scrollY + "; " + document.body.offsetHeight);

    app.savePos(window.scrollY);
    if ((window.innerHeight + window.scrollY + 10) >= document.body.offsetHeight) {
         app.endOfPage();
    }
};




//app.showtoast("please work");

package ru.polydevochki.startapp.screens.main.fragments.profile

import android.content.Intent
import android.view.View
import ru.polydevochki.startapp.screens.main.fragments.profile.proffractiv.AboutActivity

class ProfilePresenter : ProfileContract.Presenter {

    var viewIsAttached = false
    var mView: ProfileContract.View? = null
    var isEditMode = false

    private val mRepository: ProfileContract.Repository by lazy {
        ProfileRepository()
    }

    override fun attachView(view: ProfileContract.View) {
        viewIsAttached = true
        mView = view
        mRepository.attachPresenter(this)
    }

    override fun detachView() {
        viewIsAttached = false
        mView = null
    }

    override fun onBtnAboutClick(v: View) {
        val intent = Intent(v.context, AboutActivity::class.java)
        v.context.startActivity(intent)
    }

    override fun onBtnExitClick(v: View) {
        mRepository.logOut()
        mView!!.launchLogin()
    }

    override fun getNames(v: View?) {
        if (viewIsAttached) {
            mRepository.getNames(v);
        }
    }

    override fun showData(name: String, lvl: String, city: String, v: View?) {
        if (viewIsAttached)
            mView!!.showData(name, lvl, city, v)
    }

    override fun showProgress(proc: Int) {
        if (viewIsAttached)
            mView!!.showProgress(proc)
    }

    override fun onBtnEditClick() {
        isEditMode = !isEditMode
        if (isEditMode) {
            mView!!.enterEditMode()
        } else {
            mView!!.exitEditMode()
        }
    }

    override fun onNameChanged(name: String) {
        mRepository.setName(name)
    }

    override fun onCityChanged(city: String) {
        mRepository.setCity(city)
    }
}
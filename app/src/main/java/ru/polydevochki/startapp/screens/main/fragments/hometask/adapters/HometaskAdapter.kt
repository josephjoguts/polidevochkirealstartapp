package ru.polydevochki.startapp.screens.main.fragments.hometask.adapters

import android.graphics.Color
import android.transition.TransitionManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.card_hometask.view.*
import ru.polydevochki.startapp.R
import ru.polydevochki.startapp.data.Condition
import ru.polydevochki.startapp.data.HometaskData
import ru.polydevochki.startapp.App
import ru.polydevochki.startapp.utils.Utils
import java.io.File

class HometaskAdapter(
    private val mListener: OnItemInteractionListener?
) : RecyclerView.Adapter<HometaskAdapter.ViewHolder>() {
    private var models: ArrayList<HometaskData> = ArrayList()
    var rv: RecyclerView? = null
    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as HometaskData
            mListener?.onItemInteraction(item)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_hometask, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = models[position]
        holder.mModule.text = """Уровень ${item.module}. ${item.lesson}"""
        holder.mTitle.text = item.title
        holder.mTime.text = Utils.countTakenTime(item.time)
        if(File("/data/data/" + App.instance!!.packageName + "/hometasks/${item.module}/${item.title}.html").exists()){
            Log.d("file exits","file exist")
        }
        holder.mExpandedText.loadUrl("file:///data/data/" + App.instance!!.packageName + "/hometasks/${item.module}/${item.title}.html")

        when (item.status) {
            Condition.LOCKED -> {
                holder.mView.cardExpansion.visibility = View.GONE
                holder.mView.stubDone.visibility = View.GONE
                holder.mView.stubLock.visibility = View.VISIBLE
                holder.mView.openIndicator.visibility = View.VISIBLE
                holder.mView.cardview.isClickable = false
                holder.mView.tvTime.visibility = View.GONE
            }
            Condition.IN_WORK -> {
                holder.mView.stubLock.visibility = View.GONE
                holder.mView.stubDone.visibility = View.GONE
                holder.mView.tvTime.visibility = View.VISIBLE
                holder.mView.openIndicator.visibility = View.VISIBLE
                holder.mView.cardExpansion.visibility = View.GONE
            }
            Condition.DONE -> {
                holder.mView.openIndicator.visibility = View.VISIBLE
                holder.mView.cardExpansion.visibility = View.GONE
                holder.mView.stubDone.visibility = View.VISIBLE
                holder.mView.stubLock.visibility = View.GONE
                holder.mView.btnDone.visibility = View.GONE
                holder.mTitle.setTextColor(Color.LTGRAY)
                holder.mView.tvTime.visibility = View.VISIBLE
                holder.mTime.tvTime.text = "СДЕЛАНО"
            }
        }

        with(holder.mView.btnDone) {
            tag = item
            setOnClickListener(mOnClickListener)
        }

        if (item.status != Condition.LOCKED) {
            setExpansionListener(holder)
        }
    }

    private fun setExpansionListener(holder: ViewHolder) {
        holder.mView.setOnClickListener {
            TransitionManager.beginDelayedTransition(rv)
            if (holder.mView.openIndicator.visibility == View.VISIBLE) {
                holder.mView.openIndicator.visibility = View.INVISIBLE
                holder.mView.cardExpansion.visibility = View.VISIBLE
            } else {
                holder.mView.openIndicator.visibility = View.VISIBLE
                holder.mView.cardExpansion.visibility = View.GONE
            }
        }
    }

    override fun getItemCount(): Int = models.size

    override fun getItemViewType(position: Int): Int {
        return when(models[position].status){
            Condition.LOCKED -> 0
            Condition.IN_WORK -> 1
            Condition.DONE -> 2
        }
    }

    fun updateItems(items: ArrayList<HometaskData>) {
        items.sortWith(Comparator<HometaskData> { p0, p1 ->
            when (p0?.status){
                Condition.LOCKED -> when (p1?.status){
                    Condition.LOCKED -> 0
                    Condition.IN_WORK -> 1
                    Condition.DONE -> -1
                    null -> 1
                }
                Condition.IN_WORK -> when (p1?.status){
                    Condition.LOCKED -> -1
                    Condition.IN_WORK -> 0
                    Condition.DONE -> -1
                    null -> 1
                }
                Condition.DONE -> when (p1?.status){
                    Condition.LOCKED -> 1
                    Condition.IN_WORK -> 1
                    Condition.DONE -> 0
                    null -> 1
                }
                null -> 1
            }
        })
        models = items
        notifyDataSetChanged()
    }

    fun getItems(): ArrayList<HometaskData> {
        return models
    }

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mModule = mView.tvModule
        val mTitle = mView.tvTitle
        val mTime = mView.tvTime
        val mExpandedText = mView.wvExpanded
    }

    interface OnItemInteractionListener {
        fun onItemInteraction(item: HometaskData?)
    }

}
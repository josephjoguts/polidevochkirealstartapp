package ru.polydevochki.startapp.screens.main.fragments.hometask

import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.QueryDocumentSnapshot
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.google.gson.Gson
import ru.polydevochki.startapp.data.Condition
import ru.polydevochki.startapp.data.HometaskData
import ru.polydevochki.startapp.App
import ru.polydevochki.startapp.utils.Utils
import java.io.File
import java.io.FileWriter
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.schedule


class HometaskRepository: HometaskContract.Repository {
    private lateinit var mPresenter: HometaskContract.Presenter
    private lateinit var articlesCount: QuerySnapshot
    private lateinit var lvl : String
    private var made : Int = 0
    private var idflag : Boolean = false

    override fun attachPresenter(presenter: HometaskContract.Presenter) {
        mPresenter = presenter
    }


    override fun loadCardsDB(){
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        val userDoc = db.collection("users").document(userID).get()
        userDoc.addOnSuccessListener {
            val idlvl = it["lvl"].toString()
            val flag = it["lvl${idlvl}flag"].toString().toBoolean()
            val cnt = it["readed Articles"].toString().toInt()
            makelvl(idlvl,flag,cnt)
        }
    }

    override fun changeCondition(name: String) {
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        val userDoc = db.collection("users").document(userID).get()
        userDoc.addOnSuccessListener {
            lvl = it["lvl"].toString()
            made = it["readed Articles"].toString().toInt()
        }
        db.collection("users").document(userID).collection("lvl${lvl}").document(name).update("status","DONE")
        val new_cnt = ++made
        db.collection("users").document(userID).update("readed Articles",new_cnt)
        Utils.changeLevel(new_cnt)
    }

    fun sentToPres(arr :ArrayList<HometaskData>){
        mPresenter.onHomeTaskLoaded(arr)
    }

    fun makelvl(idlvl : String, flag : Boolean, cnt: Int){
        lvl = idlvl
        idflag = flag
        made = cnt
        loadToDB()
    }

    fun loadToDB(){
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        if(!idflag){
            val htasks1 = db.collection("hometasks").document("lvl${lvl}").collection("hometasks").get()
                htasks1.addOnSuccessListener {
                articlesCount = it
                for(doc in it){
                    val card = hashMapOf(
                        "lesson" to doc["lesson"].toString(),
                        "module" to doc["module"].toString(),
                        "status" to doc["status"].toString(),
                        "textExpanded" to doc["textExpanded"].toString(),
                        "time" to doc["time"].toString(),
                        "title" to doc["title"].toString())
                    db.collection("users").document(userID).collection("lvl${lvl}").document(doc["title"].toString()).set(card)
                }
            }
            db.collection("users").document(userID).update("lvl${lvl}flag",true)
        }
        Timer().schedule(500) {loadfromDB()}
    }

    fun loadfromDB(){
        var gson = Gson()
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        var arr = ArrayList<HometaskData>()
        for(i in 1 .. lvl.toInt()) {
            val htasks = db.collection("users").document(userID).collection("lvl${i}").get()
            htasks.addOnSuccessListener {
                articlesCount = it
                var item: HometaskData
                for (doc in it) {
                    val module = doc["module"].toString().toInt()
                    val lesson = doc["lesson"].toString().toInt()
                    val status = Condition.valueOf(doc["status"].toString())
                    val text = doc["textExpanded"].toString()
                    val time = gson.fromJson(doc["time"].toString(), Calendar::class.java)
                    val title = doc["title"].toString()
                    item = HometaskData(module, lesson, title, time, text, status)
                    downLoadArticle(doc!!)
                    arr.add(item)
                }
                arr.sortWith(compareByDescending<HometaskData>{it.module}.thenBy{it.lesson })
                sentToPres(arr)
            }
        }

    }

    private fun downLoadArticle(document: QueryDocumentSnapshot) {
        val stor = Firebase.storage
        val ref = stor.getReferenceFromUrl(document["textExpanded"].toString())
        ref.getBytes(1024*30).addOnSuccessListener {
            val html = String(it)
            val name = document["title"].toString()
            val path1 =
                "/data/data/" + App.instance.packageName + "/hometasks/$lvl"
            File(path1).mkdirs()
            val path = path1 + "/$name.html"
            val html_file = File(path)
            val wr = FileWriter(html_file)
            wr.append(html)
            wr.flush()
            wr.close()
            Log.d("file html",path)
        }
    }

}
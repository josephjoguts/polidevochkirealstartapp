package ru.polydevochki.startapp.screens.main.fragments.profile

import android.view.View

interface ProfileContract {

    interface View {
        abstract fun showData(name: String, lvl: String, city: String, v: android.view.View?)
        abstract fun showProgress(proc: Int)


        fun launchLogin()
        fun enterEditMode()
        fun exitEditMode()
    }

    interface Presenter {
        fun attachView(view: ProfileContract.View)
        fun detachView()
        fun onBtnAboutClick(v : android.view.View)
        fun onBtnExitClick(v : android.view.View)
        fun getNames(v:android.view.View?)
        abstract fun showData(name: String, lvl: String, city: String, v: android.view.View?)
        abstract fun showProgress(proc: Int)

        fun onBtnEditClick()
        fun onNameChanged(name: String)
        fun onCityChanged(city: String)
    }

    interface Repository {
        fun attachPresenter(presenter: ProfileContract.Presenter)
        fun logOut()
        abstract fun getNames(v: android.view.View?)
        fun setName(name: String)
        fun setCity(city: String)
    }
}
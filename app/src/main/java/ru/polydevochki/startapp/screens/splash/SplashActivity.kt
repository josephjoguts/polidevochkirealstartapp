package ru.polydevochki.startapp.screens.splash

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import ru.polydevochki.startapp.R
import ru.polydevochki.startapp.screens.login.LoginActivity
import ru.polydevochki.startapp.screens.main.MainActivity

class SplashActivity : AppCompatActivity(), SplashContract.View {
    private val mPresenter: SplashContract.Presenter by lazy {
        SplashPresenter()
    }

    override fun onStart() {
        super.onStart()
        mPresenter.attachView(this)
            //   mPresenter.loadArticles()

    }

    override fun onStop() {
        super.onStop()
        mPresenter.detachView()
    }

    override fun showMainScreen(){
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun showLoginScreen(){
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_activity)

//        Handler().postDelayed({
//            // This method will be executed once the timer is over
//            // Start your app main activity
//
//            startActivity(Intent(this,MainActivity::class.java))
//
//            // close this activity
//            finish()
//        }, SPLASH_TIME_OUT)
    }
}
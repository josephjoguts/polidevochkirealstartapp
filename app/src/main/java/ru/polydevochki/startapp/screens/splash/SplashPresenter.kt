package ru.polydevochki.startapp.screens.splash

import android.os.Handler
import android.util.Log

class SplashPresenter : SplashContract.Presenter {
    private var viewIsAttached = false
    var mView: SplashContract.View? = null
    private val mRepository: SplashContract.Repository by lazy {
        SplashRepositry()
    }

    override fun attachView(view: SplashContract.View) {
        viewIsAttached = true
        mView = view
        mRepository.attachPresenter(this)
        if (mRepository.checkAuthoriztionStatus()) {
            mView!!.showMainScreen()
        } else {
            mView!!.showLoginScreen()
        }
    }

    override fun detachView() {
        viewIsAttached = false
        mView = null
    }


}
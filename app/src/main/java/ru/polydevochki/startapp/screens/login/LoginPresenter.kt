package ru.polydevochki.startapp.screens.login

import ru.polydevochki.startapp.App
import ru.polydevochki.startapp.R
import ru.polydevochki.startapp.screens.login.LoginActivity.Companion.ToastMode
import ru.polydevochki.startapp.utils.Utils

class LoginPresenter() :
    LoginContract.Presenter {
    private var viewIsAttached = false
    var mView: LoginContract.View? = null

    private val mRepository: LoginContract.Repository by lazy {
        LoginRepository()
    }

    override fun attachView(view: LoginContract.View) {
        viewIsAttached = true
        mView = view
        mRepository.attachPresenter(this)
    }

    override fun detachView() {
        viewIsAttached = false
        mView = null
    }

    private fun checkforEmptyness(loginText: String, passText: String): Boolean {
        var result = false
        if (loginText == "") {
            mView!!.showLoginError(App.instance.getString(R.string.edit_is_empty))
            result = true
        }
        if (passText == "") {
            mView!!.showPasswordError(App.instance.getString(R.string.edit_is_empty))
            result = true
        }
        return result
    }

    override fun onBtnLoginClick(loginText: String, passText: String) {
        if (checkforEmptyness(loginText, passText))
            return
        mView!!.showLoadProgress()
        mRepository.signInAccount(loginText, passText)
    }

    override fun onBtnCreateAccountClick(login: String, password: String) {
        if (checkforEmptyness(login, password))
            return
        mView!!.showLoadProgress()
        mRepository.createAccount(login, password)
    }

    override fun onBtnResetClick(loginText: String) {
        if (loginText == "") {
            mView!!.showLoginError(App.instance.getString(R.string.edit_is_empty))
            return
        }
        mView!!.showLoadProgress()
        mRepository.resetPassword(loginText)
    }

    override fun onLoginFinished() {
        mView!!.launchMainActivity()
        mView!!.hideLoadProgress()
    }

    override fun onLoginFailed(reason: String) {
        mView!!.showToast(
            Utils.getErrorMessage(reason),
            ToastMode.ERROR
        )
        mView!!.hideLoadProgress()
    }

    override fun onRegistrationFinished() {
        mRepository.createUserDocument()
        mView!!.launchTestActivity()
        mView!!.hideLoadProgress()
    }

    override fun onRegistrationFailed(reason: String) {
        mView!!.showToast(
            Utils.getErrorMessage(reason),
            ToastMode.ERROR
        )
        mView!!.hideLoadProgress()
    }

    override fun onResetFinished() {
        mView!!.showToast(
            App.instance.getString(R.string.restore_email_sent),
            ToastMode.INFO
        )
        mView!!.enterLoginMode()
        mView!!.hideLoadProgress()
    }

    override fun onResetFailed(reason: String) {
        mView!!.showToast(
            Utils.getErrorMessage(reason),
            ToastMode.ERROR
        )
        mView!!.hideLoadProgress()
    }
}
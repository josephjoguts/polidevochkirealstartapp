package ru.polydevochki.startapp.screens.main.fragments.profile.bottomsheet

import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import kotlinx.android.synthetic.main.fragment_bottomsheetprofile.view.*
import ru.polydevochki.startapp.R
import ru.polydevochki.startapp.screens.main.fragments.profile.bottomsheet.adapters.ProfileBottomSheetAdapter
import ru.polydevochki.startapp.screens.main.fragments.profile.fromBottomSheet

class ProfileBottomSheetDialog(fromBottomSheet:fromBottomSheet): BottomSheetDialogFragment(),ProfileBottomSheetContract.Viev {
    lateinit var v :View
    private var fromBottomSheet: fromBottomSheet
    init {
        this.fromBottomSheet = fromBottomSheet
    }
    private val mPresenter by lazy {ProfileBottomSheetPresenter()  }
    override fun onAttach(context: Context) {
        super.onAttach(context)
        mPresenter.attachView(this);
    }

    override fun onDetach() {
        super.onDetach()
        mPresenter.detachView()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v  = inflater.inflate(R.layout.fragment_bottomsheetprofile,null);
        val rv=v.findViewById<RecyclerView>(R.id.rv_bottomsheetdialog_images)
        setUpRv(rv)
       // Log.d("RecyclerViewImages","${t}")
        v.toolbar.setNavigationOnClickListener {
            dismiss()
        }
        dialog!!.setOnShowListener(DialogInterface.OnShowListener { dialog ->
            val d = dialog as BottomSheetDialog
            val bottomSheet =
                d.findViewById(R.id.design_bottom_sheet) as FrameLayout? ?: return@OnShowListener
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet)
            bottomSheet.background = null
        })
        return v
    }

    fun setUpRv(rv:RecyclerView) {
        val layoutManager = LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL,false)
        rv.layoutManager=layoutManager
        rv.adapter = ProfileBottomSheetAdapter( mPresenter.loadImages(),fromBottomSheet)
    }


}
package ru.polydevochki.startapp.screens.testing

import ru.polydevochki.startapp.App
import ru.polydevochki.startapp.R
import ru.polydevochki.startapp.data.QuestionModel

class TestPresenter : TestContract.Presenter {
    private var viewIsAttached = false
    var mView: TestContract.View? = null

    private val mRepository: TestContract.Repository by lazy {
        TestRepository()
    }

    override fun attachView(view: TestContract.View) {
        viewIsAttached = true
        mView = view
        mRepository.attachPresenter(this)
    }

    override fun detachView() {
        viewIsAttached = false
        mView = null
    }

    override fun getQuestions(){
        mRepository.getQuestions()
    }

    override fun onQuestionsLoaded(data: ArrayList<QuestionModel>) {
        mView!!.initTest(data)
    }

    private fun checkforEmptyness(nameText: String, cityText: String): Boolean {
        var result = false
        if (nameText == "") {
            mView!!.showNameError(App.instance.getString(R.string.edit_is_empty))
            result = true
        }
        if (cityText == "") {
            mView!!.showCityError(App.instance.getString(R.string.edit_is_empty))
            result = true
        }
        return result
    }

    override fun onBtnSendClicked(name: String, city: String) {
        if (checkforEmptyness(name, city))
            return
        mRepository.onUserDataGot(name, city)
        mView!!.startTest()
    }

    override fun onBtnEndClick(level: Int){
        mRepository.onLevelGot(level)
        mView!!.launchMainActivity()
    }
}
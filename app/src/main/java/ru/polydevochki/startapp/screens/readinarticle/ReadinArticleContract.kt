package ru.polydevochki.startapp.screens.readinarticle

interface ReadinArticleContract {

    interface View{

    }

    interface Presenter{
        fun attachView(view: View)
        fun detachView()
        fun changeCard(name : String)
        fun checkCond(name:String,lvl:String)
    }

    interface Repository{
        fun changeCardDB(name: String)
        fun attachPresenter(presenter: Presenter)
        fun checkCondDB(name:String,lvl:String)
    }
}
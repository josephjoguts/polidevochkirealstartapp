package ru.polydevochki.startapp.screens.splash

interface SplashContract {

    interface View{
        fun showMainScreen()
        fun showLoginScreen()
    }

    interface Presenter{
        fun attachView(view: View)
        fun detachView()
    }

    interface Repository{

        fun attachPresenter(presenter: Presenter)
        fun checkAuthoriztionStatus(): Boolean
        fun loadArticles()
    }
}
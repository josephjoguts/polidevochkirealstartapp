package ru.polydevochki.startapp.screens.login

import android.content.ContentValues.TAG
import android.util.Log
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthException
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import ru.polydevochki.startapp.App


class LoginRepository : LoginContract.Repository {

    private lateinit var mPresenter: LoginContract.Presenter

    private lateinit var auth: FirebaseAuth


    override fun checkAccount(): Boolean {
        return auth.currentUser != null
    }

    override fun attachPresenter(presenter: LoginContract.Presenter) {
        mPresenter = presenter
        auth = FirebaseAuth.getInstance()
    }


    override fun createAccount(email: String, password: String) {
        auth.createUserWithEmailAndPassword(email, password)
            .addOnSuccessListener { task ->
                mPresenter.onRegistrationFinished()
                task.additionalUserInfo
                createUserDocument()
            }.addOnFailureListener {
                if (it is FirebaseAuthException)
                    mPresenter.onLoginFailed(it.errorCode)
                else if (it is FirebaseNetworkException)
                    mPresenter.onLoginFailed("ERROR_NETWORK")
                else {
                    mPresenter.onLoginFailed(it.toString())
                }
            }
    }

    override fun signInAccount(email: String, password: String) {
        auth.signInWithEmailAndPassword(email, password)
            .addOnSuccessListener {
                val db = Firebase.firestore
                val userID = FirebaseAuth.getInstance().currentUser!!.email!!
                val ref =   db.collection("users").document(userID)
                ref.get().addOnSuccessListener{
                    if(it!=null) {
                        val lvl = it.get("lvl").toString()
                        App.setPrefs("lvl","lvl${lvl}")
                        mPresenter.onLoginFinished()
                    }

                }
            }.addOnFailureListener {
                if (it is FirebaseAuthException)
                    mPresenter.onLoginFailed(it.errorCode)
                else if (it is FirebaseNetworkException)
                    mPresenter.onLoginFailed("ERROR_NETWORK")
                else {
                    mPresenter.onLoginFailed(it.toString())
                }
            }
    }

    override fun createUserDocument() {
        val user = auth.currentUser
        val db = Firebase.firestore
        val userProps = hashMapOf(
            "lvl" to 1,
            "lvl1flag" to false,
            "lvl2flag" to false,
            "lvl3flag" to false,
            "lvl4flag" to false,
            "lvl5flag" to false,
            "readed Articles" to 0,
            "name" to "",
            "city" to ""
        )
        db.collection("users").document(user?.email!!).set(userProps)
            .addOnSuccessListener { Log.d(TAG, "DocumentSnapshot successfully written!") }
            .addOnFailureListener { e -> Log.w(TAG, "Error writing document", e) }
    }

    override fun resetPassword(email: String) {
        auth.sendPasswordResetEmail(email).addOnSuccessListener { task ->
            mPresenter.onResetFinished()
        }.addOnFailureListener {
            if (it is FirebaseAuthException)
                mPresenter.onResetFailed(it.errorCode)
            else if (it is FirebaseNetworkException)
                mPresenter.onResetFailed("ERROR_NETWORK")
            else {
                mPresenter.onResetFailed(it.toString())
            }
        }
    }

}


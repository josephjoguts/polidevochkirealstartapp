package ru.polydevochki.startapp.screens.main

import android.util.Log
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import ru.polydevochki.startapp.App
import java.io.File
import java.io.FileWriter

class MainRepository: MainContract.Repository {
    private lateinit var  mPresenter: MainContract.Presenter
    override fun loadLibs() {
        val context = App.instance

        val path1 = "/data/data/" + context.packageName + "/articles/libs"
        var file = File(path1)
        Log.d("i exist t","${file.exists()}")
        if (!file.exists()) {
            file.mkdirs();



            val store = Firebase.storage
            val refJquery = store.getReference("articles/libs/jquery-3.4.1.min.js")
            val refJs = store.getReference("articles/libs/jscode.js")
            val refStyle = store.getReference("articles/libs/style.css")
            refJquery.getBytes(1024 * 1024).addOnSuccessListener { x ->
                downloadJquery(path1,x)
            }.addOnFailureListener { Log.d("jquerryDownload","unsuccess") }
            refJs.getBytes(1024 * 1024).addOnSuccessListener { x ->
                downloadJs(path1,x)
            }.addOnFailureListener { Log.d("jsDownload","unsuccess") }
            refStyle.getBytes(1024 * 1024).addOnSuccessListener { x ->
                downloadStyle(path1,x)
            }.addOnFailureListener { Log.d("styleDownload","unsuccess") }



        }
    }

    private fun downloadStyle(path1: String, x: ByteArray) {
        Log.d("styleDownload","success")
        val path = path1+"/style.css"
        val style = String(x);
        val style_file = File(path)
        val wr = FileWriter(style_file)
        wr.append(style)
        wr.flush()
        wr.close()
        Log.d("path",File("/data/data/${App.instance.packageName}/articles/libs").exists().toString())

    }

    private fun downloadJs(path1: String, x: ByteArray) {
        Log.d("jsDownload","success")
        val path = path1+"/jscode.js"
        val js = String(x);
        val js_file = File(path)
        val wr = FileWriter(js_file)

        wr.append(js)
        wr.flush()
        wr.close()
        Log.d("path",File("/data/data/${App.instance.packageName}/articles/libs").exists().toString())
    }

    private fun downloadJquery(path1:String,x:ByteArray) {
        Log.d("jquerryDownload","success")
        val path = path1+"/jquery-3.4.1.min.js"
        val jquerry = String(x);
        val jquerry_file = File(path)
        val wr = FileWriter(jquerry_file)
        wr.append(jquerry)
        wr.flush()
        wr.close()
        Log.d("path",File("/data/data/${App.instance.packageName}/articles/libs").exists().toString())
    }

    override fun attachPresenter(presenter: MainContract.Presenter) {
            mPresenter = presenter
        }
    }
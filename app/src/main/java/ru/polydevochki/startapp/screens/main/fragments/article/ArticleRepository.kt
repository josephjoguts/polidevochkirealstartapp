package ru.polydevochki.startapp.screens.main.fragments.article

import android.content.Context
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QueryDocumentSnapshot
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import ru.polydevochki.startapp.data.ArticleCard
import ru.polydevochki.startapp.App
import java.io.File
import java.io.FileOutputStream
import java.io.FileWriter

class ArticleRepository:ArticleContract.Repository {
    private lateinit var lvl:String
    private lateinit var basePath:String
    private lateinit var imagesPath:String
    private lateinit var db: FirebaseFirestore
    private lateinit var context:Context
    private lateinit var stor: FirebaseStorage
    private lateinit var mPresenter: ArticleContract.Presenter
    override fun attachPresenter(presenter: ArticleContract.Presenter) {
        mPresenter = presenter;
    }
    //TODO(сделать сохранение фоточек)
    override fun loadInfo(context: Context?, changeType: Int) {
        val context =  App.instance;

        lvl = App.instance.getSharedPreferences("Articles",0).getString("lvl","lvl1").toString()
        Log.d("loadInfo",changeType.toString())
        basePath = "/data/data/" + context!!.packageName + "/articles/$lvl"
        imagesPath=basePath+"/images"
        val t = File(basePath)
        if(changeType>0)
        {
            Log.d("changeType",changeType.toString())
            File("data/data/${context!!.packageName}/articles/lvl$changeType").deleteRecursively()
           loadArticles(changeType)
        }
        else {
            Log.d("filesEmpty", "${t.exists()}")
            if (!t.exists()) {
                Log.d("test", "Current data:${t}")
                val lvlsCount = lvl.split("vl")[1].toInt()
                for (i in 1..lvlsCount)
                    if (!File("/data/data/${context!!.packageName}/articles/lvl$i").exists())
                        loadArticles(i)
            } else
                loadFromMemory()
        }
    }


    private fun loadFromMemory() {
        Log.d("memory","")
        val context =  App.instance;
        var arr = mutableListOf<ArticleCard>()
     //   var t = File(basePath)
        var c = "/data/data/" + context!!.packageName + "/articles"
        val packag = File(c)
        Log.d("noInternet","test")
        val lvlCheck = lvl.split("vl")[1].toInt()
        for(t in packag.listFiles().filter { file -> !(file.name.equals("libs")) }){
         if(t.name.split("vl")[1].toInt()<=lvlCheck)
         for (f in t.listFiles().filter {x->!x.name.equals("images")})
         {
             val userID = FirebaseAuth.getInstance().currentUser!!.email!!
            val info = File(f.absolutePath+"/info.txt");
            val lines = info.readLines();
            val name = lines[0]
            val timeToRead = lines[1]
            val module = lines[2]
            val imagePath =lines[3]
            val htmlPath = f.listFiles().filter { x->!x.nameWithoutExtension.equals("info")}[0].absolutePath
            val check =
                context.getSharedPreferences("Articles", 0).getBoolean(htmlPath+userID, false)
            arr.add(ArticleCard(imagePath,name,timeToRead,check,module,htmlPath));
           }
        }
       // Log.d("second",File("/data/data/${MyApp.instance.packageName}/articles/libs").exists().toString())
        mPresenter.loadAdapter(arr.toTypedArray(),0)

    }


    fun loadArticles(loadedLvl:Int)
    {

        db = Firebase.firestore
        stor = Firebase.storage;
        context =  App.instance;
        File("/data/data/" + context!!.packageName + "/articles/lvl$loadedLvl").mkdirs()
        val downLoad=db.collection("articles").document("lvl$loadedLvl").collection("articles").get();
        downLoad.addOnSuccessListener{
            val articlesCount = it.size().toInt()
            for(document in it)
            {
                if (!document["image"].toString().equals("null"))
                {
                    downloadImage(document["image"].toString(),loadedLvl,articlesCount)
                }
                if(!document["path"].toString().equals("null"))
                {
                    downLoadArticle(document,loadedLvl,articlesCount)
                }

            }

        }

    }
    private fun downLoadArticle(document: QueryDocumentSnapshot,loadedLvl: Int,articlesCount:Int) {
        val ref = stor.getReferenceFromUrl(document["path"].toString())
            ref.getBytes(1024*30).addOnSuccessListener{
                val html = String(it)
            val name = document["name"].toString()
            val timeTOread = document["time"].toString()
            val module = document["lvl"].toString()
            val imageUri = document["image"].toString()
            val path1 =
                "/data/data/" + context.packageName + "/articles/lvl$loadedLvl/$name";
            if(!(name.equals("null")||timeTOread.equals("null")||module.equals("null")||imageUri.equals("null")))
            {
                File(path1).mkdirs()
                val path=path1+"/$name.html"
                val txtP=path1+"/info.txt"
                val imagePath = "/data/data/" + context!!.packageName + "/articles/lvl$loadedLvl/images"+"/"+imageUri.split("/lvl$loadedLvl/images/")[1]
                val html_file = File(path)
                var txt = File(txtP);
                txt.writeText("$name\n$timeTOread\n$module\n$imagePath")
                val wr = FileWriter(html_file)
                wr.append(html)
                wr.flush()
                wr.close()
                Log.d("downloadedArt",path)
                checkReady(loadedLvl.toString(),articlesCount)
            }

        }.addOnFailureListener {
            Log.d("Failure",it.toString())
        }.addOnCanceledListener{
            Log.d("CanceledListener","canceled")
        }


    }

    private fun downloadImage(imageUri: String,loadedLvl: Int,articlesCount:Int) {

        val ref = stor.getReferenceFromUrl(imageUri)
        ref.getBytes(1024*1024*4).addOnSuccessListener{
            val t=  "/data/data/" + context!!.packageName + "/articles/lvl$loadedLvl/images"
            File(t).mkdirs()
            Log.d("imageUri",imageUri)
            val localImagePath = t+"/"+imageUri.split("/lvl$loadedLvl/images/")[1]
            val wr = FileOutputStream(File(localImagePath))
            Log.d("downloaded",localImagePath)
            wr.write(it)
            wr.flush()
            wr.close()
            checkReady(loadedLvl.toString(),articlesCount)
        }

    }

    private fun checkReady(loadedLvl:String,articlesCount: Int) {
        val t = File("/data/data/" + context!!.packageName + "/articles/lvl$loadedLvl")
            // Log.d("filesSize",t.listFiles().size.toString())
        if(t.listFiles().size==articlesCount+1)
        {
            if(File("/data/data/" + context!!.packageName + "/articles/lvl$loadedLvl/images").listFiles().size==articlesCount) {
                Log.d("filesSize", t.listFiles().size.toString())
                loadFromMemory()
            }
        }
    }

}
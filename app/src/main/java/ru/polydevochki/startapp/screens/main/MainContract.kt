package ru.polydevochki.startapp.screens.main

interface MainContract {
    interface View{

    }

    interface Presenter{
        fun attachView(view: View)
        fun detachView()
        fun loadLibs();
    }

    interface Repository{
        fun loadLibs()
        fun attachPresenter(presenter: Presenter)
    }
}
package ru.polydevochki.startapp.screens.testing.adapters

import android.view.View
import kotlinx.android.synthetic.main.activity_test.view.*
import kotlinx.android.synthetic.main.item_message.view.*
import ru.polydevochki.startapp.data.AnswerModel
import ru.polydevochki.startapp.data.QuestionModel

class AnswersAdapter(
    private val mView: View,
    private val callBack: AnswerCallBack
) {
    private var questionList = ArrayList<QuestionModel>()
    private var currentQuestion = 0

    fun updateAnswers(list: ArrayList<AnswerModel>) {
        mView.option1.apply {
            visibility = View.VISIBLE
            tvTextMessage.text = list[0].text
            cvMessageBackground.setOnClickListener {
                callBack.onAnswerSelected(list[0])
            }
        }
        mView.option2.apply {
            visibility = View.VISIBLE
            tvTextMessage.text = list[1].text
            cvMessageBackground.setOnClickListener {
                callBack.onAnswerSelected(list[1])
            }
        }
        if (list.size == 3) {
            mView.option3.apply {
                visibility = View.VISIBLE
                tvTextMessage.text = list[2].text
                cvMessageBackground.setOnClickListener {
                    callBack.onAnswerSelected(list[2])
                }
            }
        } else {
            mView.option3.visibility = View.GONE
        }
    }

    fun updateQuestions(items: ArrayList<QuestionModel>) {
        questionList = items
    }

    fun nextQuestion(): String {
        currentQuestion++
        if (currentQuestion > questionList.size) {
            return ""
        }
        updateAnswers(questionList[currentQuestion - 1].answers)
        return questionList[currentQuestion - 1].text
    }

    fun getItemCount():Int{
        return questionList.size
    }

    fun getCurrentQuestionNumber(): Int{
        return currentQuestion
    }
    interface AnswerCallBack {
        fun onAnswerSelected(answer: AnswerModel)
    }
}
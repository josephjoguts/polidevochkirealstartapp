package ru.polydevochki.startapp.screens.testing.adapters

import android.graphics.Color
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_message.view.*
import ru.polydevochki.startapp.App
import ru.polydevochki.startapp.R
import ru.polydevochki.startapp.data.MessageModel

class ChatHistoryAdapter(
) : RecyclerView.Adapter<ChatHistoryAdapter.ViewHolder>() {
    private var models: ArrayList<MessageModel> = ArrayList()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_message, parent, false)
        if (viewType == 0) {
            return OuterViewHolder(view)
        } else {
            view.cvMessageBackground.setCardBackgroundColor(App.instance.getColor(R.color.inner_message))
            view.llWrap.gravity = Gravity.START
            view.tvTextMessage.setTextColor(Color.BLACK)
            return InnerViewHolder(view)
        }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = models[position]
        holder.mText.text = item.text
    }

    override fun getItemCount(): Int = models.size

    fun addItem(message: String, isInner: Boolean) {
        models.add(MessageModel(message, isInner))
        notifyItemInserted(models.size - 1)
    }

    override fun getItemViewType(position: Int): Int {
        return if (models[position].isInner) 1 else 0
    }

    open inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mText = mView.tvTextMessage
    }

    inner class InnerViewHolder(view: View) : ViewHolder(view)
    inner class OuterViewHolder(view: View) : ViewHolder(view)
}
package ru.polydevochki.startapp.screens.main.fragments.profile

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.Fragment
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.edit_dialog.view.*
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_profile.view.*
import kotlinx.android.synthetic.main.item_profile_options.view.*
import ru.polydevochki.startapp.App
import ru.polydevochki.startapp.R
import ru.polydevochki.startapp.screens.login.LoginActivity
import ru.polydevochki.startapp.screens.main.fragments.profile.bottomsheet.ProfileBottomSheetDialog

interface fromBottomSheet {
    fun showImage(d: Drawable)
}

class ProfileFragment : Fragment(), ProfileContract.View, fromBottomSheet {
    private val mPresenter by lazy { ProfilePresenter() }
    private lateinit var v: View
    private val bottomSheet by lazy { ProfileBottomSheetDialog(this) }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        v = inflater.inflate(R.layout.fragment_profile, null);
        setupOptions(v)
        setUpBtns(v)
        setUpNames(v)
        return v

    }


    private fun setUpNames(v: View?) {
        mPresenter.getNames(v);

    }

    private fun setupOptions(v: View?) {
        v!!.optAbout.tvOptionText.text = "О приложении"
        v.optExit.tvOptionText.text = "Выйти"
        v.optExit.tvOptionText.setTextColor(Color.RED)
        v.ivAvatar.clipToOutline = true
    }

    override fun onStart() {
        super.onStart()
        val context = App.instance
        val lvlDelete = context.getSharedPreferences("Articles", 0).getString("lvl", "lvl1")
        lvl.setText(lvlDelete?.get(3).toString())
        val inpS = context.assets.open(App.getPrefs("avatar", "images/test/no-avatar.png"))
        val drawable = Drawable.createFromStream(inpS, null)
        ivAvatar.setImageDrawable(drawable)
        btnEditAvatar.setOnClickListener {
            bottomSheet.show(childFragmentManager, "bottomSheetImages")
        }
        btnEditName.setOnClickListener {
            val view = layoutInflater.inflate(R.layout.edit_dialog,null)
            val dialog = AlertDialog.Builder(activity)
            dialog.setView(view)
            dialog.setTitle("Изменить имя")
            val realDialog = dialog.create()
            view.btnSave.setOnClickListener {
                mPresenter.onNameChanged(view.etNew.text.toString())
                tvName.text = view.etNew.text.toString()
                realDialog.cancel()
            }
            view.btnCancel.setOnClickListener {
                realDialog.cancel()
            }
            realDialog.show()
        }
        btnEditCity.setOnClickListener {
            val view = layoutInflater.inflate(R.layout.edit_dialog,null)
            val dialog = AlertDialog.Builder(activity)
            dialog.setView(view)
            dialog.setTitle("Изменить город")
            val realDialog = dialog.create()
            view.btnSave.setOnClickListener {
                mPresenter.onCityChanged(view.etNew.text.toString())
                tvCity.text = view.etNew.text.toString()
                realDialog.cancel()
            }
            view.btnCancel.setOnClickListener {
                realDialog.cancel()
            }
            realDialog.show()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mPresenter.attachView(this);
    }

    override fun onDetach() {
        super.onDetach()
        mPresenter.detachView()
    }

    fun setUpBtns(v: View?) {
        v?.optAbout?.llOption?.setOnClickListener {
            mPresenter.onBtnAboutClick(v)
        }

        v?.optExit?.llOption?.setOnClickListener {
            mPresenter.onBtnExitClick(v)
        }
        v!!.btnEdit.setOnClickListener {
            mPresenter.onBtnEditClick()
        }
    }

    override fun launchLogin(){
        val intent = Intent(context, LoginActivity::class.java)
        startActivity(intent)
        Toasty.success(App.instance,"Вы вышли из аккаунта", Toast.LENGTH_SHORT,true).show()
    }

    override fun showData(name: String, lvl: String, city: String, v: View?) {
        pbProfile.visibility = View.GONE
        llInfo.visibility = View.VISIBLE
        llStats.visibility = View.VISIBLE
        tvName.text = name
        v!!.findViewById<TextView>(R.id.lvl).text = lvl;
        tvCity.text = city
    }

    override fun showProgress(proc: Int) {
        tvProgress.text = proc.toString() + "%"
    }

    override fun showImage(d: Drawable) {
        ivAvatar.setImageDrawable(d)
        bottomSheet.dismiss()
    }

    override fun enterEditMode(){
        btnEdit.setImageResource(R.drawable.ic_close_black_24dp)
        btnEditAvatar.visibility = View.VISIBLE
        btnEditCity.visibility = View.VISIBLE
        btnEditName.visibility = View.VISIBLE
    }

    override fun exitEditMode(){
        btnEdit.setImageResource(R.drawable.ic_pen_black_24)
        btnEditAvatar.visibility = View.GONE
        btnEditCity.visibility = View.GONE
        btnEditName.visibility = View.GONE
    }

}
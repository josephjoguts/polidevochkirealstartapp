package ru.polydevochki.startapp.screens.readinarticle

import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.google.gson.Gson
import es.dmoral.toasty.Toasty
import ru.polydevochki.startapp.App
import ru.polydevochki.startapp.data.Condition
import ru.polydevochki.startapp.data.HometaskData
import ru.polydevochki.startapp.screens.splash.SplashContract
import java.util.*
import kotlin.concurrent.schedule

class ReadinArticleRepository:ReadinArticleContract.Repository {

    private lateinit var  mPresenter: ReadinArticleContract.Presenter

    override fun attachPresenter(presenter: ReadinArticleContract.Presenter) {
        mPresenter = presenter
    }

    override fun checkCondDB(name: String, lvl: String) {
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        val lvl1 = lvl[lvl.length - 3]
        val htasks = db.collection("users").document(userID).collection("lvl${lvl1}").document(name).get()
        htasks.addOnSuccessListener {
            val cond = Condition.valueOf(it["status"].toString())
            if(cond == Condition.LOCKED){
                db.collection("users").document(userID).collection("lvl${lvl1}").document(name).update("status","IN_WORK")
                var jsonString = Gson().toJson(Calendar.getInstance())
                db.collection("users").document(userID).collection("lvl${lvl1}").document(name).update("time",jsonString)
                Toasty.info(App.instance,"Время делать домашку!",Toast.LENGTH_SHORT,true).show()
            }
        }
    }

    override fun changeCardDB(name: String){
        var gson = Gson()
        var lvl = "1"
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        val userDoc = db.collection("users").document(userID).get()
        userDoc.addOnSuccessListener {
            lvl = it["lvl"].toString()
        }
        Timer().schedule(500){
            db.collection("users").document(userID).collection("lvl${lvl}").document(name).update("status","IN_WORK")
            var jsonString = gson.toJson(Calendar.getInstance())
            db.collection("users").document(userID).collection("lvl${lvl}").document(name).update("time",jsonString)
        }
    }
}
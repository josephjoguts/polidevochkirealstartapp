package ru.polydevochki.startapp.screens.main.fragments.profile.bottomsheet

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ru.polydevochki.startapp.data.BotttomSheetDrawableModel

class ProfileBottomSheetPresenter:ProfileBottomSheetContract.Presenter {
    var viewIsAttached = false
    var mView: ProfileBottomSheetContract.Viev? = null
    private val mRepository: ProfileBottomSheetContract.Repository by lazy { ProfileBottomSheetRepository() }

    override fun attachView(view: ProfileBottomSheetContract.Viev) {
        viewIsAttached = true
        mView = view
        mRepository.attachPresenter(this)
    }

    override fun detachView() {
        viewIsAttached = false
        mView = null
    }

    override fun loadImages():  Array<BotttomSheetDrawableModel> {
       return mRepository.loadImages()
    }
}
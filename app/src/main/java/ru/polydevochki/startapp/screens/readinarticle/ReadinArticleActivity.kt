package ru.polydevochki.startapp.screens.readinarticle

import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.JavascriptInterface
import android.webkit.WebChromeClient
import android.webkit.WebSettings
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_readin_article.*
import ru.polydevochki.startapp.R
import ru.polydevochki.startapp.App
import java.io.File


class ReadinArticleActivity : AppCompatActivity(),ReadinArticleContract.View {

    lateinit var path:String
    lateinit var name:String
    lateinit var lvl: String
    lateinit var savePos:String
    lateinit var articleName: String
    private val mPresenter: ReadinArticleContract.Presenter by lazy {
        ReadinArticlePresenter()
    }

    override fun onStart() {
        super.onStart()

        mPresenter.attachView(this)
    }

    override fun onStop() {
        super.onStop()
        mPresenter.detachView()
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        val editor = getSharedPreferences("Article", 0).edit()
        editor.putString(path + "SAVED_POS"+userID, (savePos.toDouble()-10).toString()).apply()

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_readin_article)
        setupListeners()
        path = intent.getStringExtra("ARTICLE_PATH")
        name = intent.getStringExtra("ARTICLE_NAME")
        lvl = intent.getStringExtra("ARTICLE_LVL")
        toolbar.title = name
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        savePos = getSharedPreferences("Article",0).getString(path+"SAVED_POS"+userID,"0").toString()
        articlescreen.settings.javaScriptEnabled = true;
        articlescreen.webChromeClient = WebChromeClient();
        articlescreen.addJavascriptInterface(JavaScriptInterface(), "app");
        Log.d("read",File("/data/data/${App.instance.packageName}/articles/lvl1/Выбор ниши").exists().toString())
        val file = File(path)
        Log.d("read",path)
        val webSettings: WebSettings = articlescreen.getSettings()
        webSettings.defaultTextEncodingName = "utf-8"
        webSettings.loadWithOverviewMode=true
        webSettings.useWideViewPort=true
        articlescreen.setInitialScale(1)
        articlescreen.loadUrl("file:///$file?$savePos")

    }

    private inner class JavaScriptInterface {
        @JavascriptInterface
        fun showtoast(fromWeb: String) {
            Toast.makeText(this@ReadinArticleActivity,fromWeb,Toast.LENGTH_LONG).show()
        }
        @JavascriptInterface
        fun endOfPage() {
            val userID = FirebaseAuth.getInstance().currentUser!!.email!!
            if(!getSharedPreferences("Articles",0).getBoolean(path+userID,false)){
            Toasty.info(this@ReadinArticleActivity,"Время делать домашку!",Toast.LENGTH_SHORT,true).show()
            endOfPageInteraction()
            } else {
                mPresenter.checkCond(name,lvl)
            }
        }
        @JavascriptInterface
        fun savePos(pos:String)
        {
            savePos=pos
            Log.i("savePosWorks","test")

        }
    }
    fun endOfPageInteraction()
    {
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        getSharedPreferences("Articles",0).edit().putBoolean(path+userID,true).apply()
        mPresenter.changeCard(name)
    }

    private fun setupListeners() {
        toolbar.setNavigationOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                finish()
            }
        })
    }



}

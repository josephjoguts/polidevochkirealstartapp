package ru.polydevochki.startapp.screens.readinarticle

import ru.polydevochki.startapp.screens.splash.SplashContract
import ru.polydevochki.startapp.screens.splash.SplashRepositry

class ReadinArticlePresenter:ReadinArticleContract.Presenter {
    private var viewIsAttached = false
    var mView: ReadinArticleContract.View? = null
    private val mRepository: ReadinArticleContract.Repository by lazy {
        ReadinArticleRepository()
    }

    override fun attachView(view: ReadinArticleContract.View) {
        viewIsAttached = true
        mView = view
        mRepository.attachPresenter(this)
    }

    override fun detachView() {
        viewIsAttached = false
        mView = null
    }

    override fun changeCard(name: String) {
        mRepository.changeCardDB(name)
    }

    override fun checkCond(name: String, lvl: String) {
        mRepository.checkCondDB(name,lvl)
    }
}

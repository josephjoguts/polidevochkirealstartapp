package ru.polydevochki.startapp.screens.main.fragments.article

import android.content.Context
import ru.polydevochki.startapp.data.ArticleCard

class ArticlePresenter : ArticleContract.Presenter {
    var viewIsAttached = false
    var mView: ArticleContract.View? = null
    private val mRepository: ArticleContract.Repository by lazy {
        ArticleRepository()
    }

    override fun attachView(view: ArticleContract.View) {
        viewIsAttached = true
        mView = view
        mRepository.attachPresenter(this)
    }

    override fun detachView() {
        viewIsAttached = false
        mView = null
    }

    override fun loadInfo(context: Context?, changeType: Int) {
        if (viewIsAttached)
            mRepository.loadInfo(context, changeType)
    }

    override fun loadAdapter(
        arr: Array<ArticleCard>,
        articleCount: Int
    ) {
        if (viewIsAttached) {
            arr.sortWith(compareByDescending<ArticleCard> { it.Module.split(".")[0] }.thenBy {
                it.Module.split(
                    "."
                )[1]
            })
            mView?.showRecycler(arr, articleCount);
        }
    }


}
package ru.polydevochki.startapp.screens.main

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import androidx.navigation.ui.setupWithNavController
import kotlinx.android.synthetic.main.activity_main.*
import ru.polydevochki.startapp.R

class MainActivity : AppCompatActivity(), MainContract.View {

    private val mPresenter: MainContract.Presenter by lazy {
        MainPresenter()
    }

    override fun onStart() {
        super.onStart()
        mPresenter.attachView(this)
    }

    override fun onStop() {
        super.onStop()
        mPresenter.detachView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottom_nav.setupWithNavController(findNavController(R.id.main_frame))
        loadLibs()
    }

    private fun loadLibs() {
        mPresenter.loadLibs()
    }
}

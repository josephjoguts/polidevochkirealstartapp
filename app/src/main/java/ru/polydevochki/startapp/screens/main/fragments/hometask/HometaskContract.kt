package ru.polydevochki.startapp.screens.main.fragments.hometask

import android.content.Context
import ru.polydevochki.startapp.data.HometaskData


interface HometaskContract {

    interface View {
        fun updateHometask(items: ArrayList<HometaskData>)
    }

    interface Presenter {
        fun attachView(view: HometaskContract.View)
        fun detachView()
        fun onHomeTaskLoaded(items: ArrayList<HometaskData>)
        fun loadInfo()
        fun changeCond(item: HometaskData)
    }

    interface Repository {
        fun attachPresenter(presenter: HometaskContract.Presenter)
        fun loadCardsDB()
        fun changeCondition(name: String)
    }
}
package ru.polydevochki.startapp.screens.splash

import com.google.firebase.auth.FirebaseAuth

import android.content.Context
import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QueryDocumentSnapshot
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.ktx.storage
import ru.polydevochki.startapp.App
import java.io.File
import java.io.FileOutputStream
import java.io.FileWriter

class SplashRepositry : SplashContract.Repository {
    private lateinit var lvl:String
    private lateinit var db:FirebaseFirestore
    private lateinit var context:Context
    private lateinit var stor:FirebaseStorage
    private lateinit var articlesCount:QuerySnapshot
    private lateinit var mPresenter: SplashContract.Presenter
    private lateinit var auth: FirebaseAuth
    override fun attachPresenter(presenter: SplashContract.Presenter) {
        mPresenter = presenter
    }

    override fun loadArticles()
    {
        db = Firebase.firestore;
        stor = Firebase.storage;
        context =  App.instance;
        lvl = context.getSharedPreferences("Articles",0).getString("lvl","lvl1").toString()
        val imagePath1="/data/data/"+context.packageName+"/article/$lvl/images"
        File(imagePath1).mkdirs()
        val path1 =
            "/data/data/" + context.packageName + "/articles/$lvl";
        File(path1).mkdirs()
        val downLooad=db.collection("articles").document(lvl).collection("articles").get();
        downLooad.addOnSuccessListener{
            articlesCount= it
            for(document in it)
            {
                if (!document["image"].toString().equals("null"))
                {
                    downloadImage(document["image"].toString())

                }
                if(!document["path"].toString().equals("null"))
                {
                    downLoadArticle(document)
                }
            }
        }
    }
    private fun checkReady()
    {
        val imagePath1="/data/data/"+context.packageName+"/article/$lvl/images/"
        val path1 =
            "/data/data/" + context.packageName + "/articles/$lvl"
        val imagefile= File(imagePath1)
        Log.d("ready", "allready")
        val articlesFile = File(path1)
        Log.d("articlefiles","articles:${articlesFile.listFiles().size}")
        Log.d("imagefiles","image:${articlesFile.listFiles()[0].absolutePath}")

            if (imagefile.listFiles().size == articlesCount.size()) {
                Log.d("unreayready","allready")
                if (articlesFile.listFiles().size + 1 >= articlesCount.size()) {
                    Log.d("ready", "allready")
                }
            }



    }

    private fun downLoadArticle(document: QueryDocumentSnapshot) {
        val ref = stor.getReferenceFromUrl(document["path"].toString())
        ref.getBytes(1024*30).addOnSuccessListener{
            val html = String(it)
            val name = document["name"].toString()
            val timeTOread = document["time"].toString()
            val module = document["lvl"].toString()
            val imageUri = document["image"].toString()
            val imagePath1="/data/data"+context.packageName+"/article/$lvl/image/"
            val path1 =
                "/data/data/" + context.packageName + "/articles/$lvl/$name";
            if(!(name.equals("null")||timeTOread.equals("null")||module.equals("null")||imageUri.equals("null")))
            {
                File(path1).mkdirs()
                val path=path1+"/$name.html"
                val txtP=path1+"/info.txt"
                val imagePath = imagePath1+"/"+imageUri.split("/$lvl/images/")[1]
                val html_file = File(path)
                var txt = File(txtP);
                txt.writeText("$name\n$timeTOread\n$module\n$imagePath")
                val wr = FileWriter(html_file)
                wr.append(html)
                wr.flush()
                wr.close()
            }
            checkReady()
        }


    }

    private fun downloadImage(imageUri: String) {
        val imagePath1="/data/data/"+context.packageName+"/article/$lvl/images"
        File(imagePath1).mkdirs()
        val ref = stor.getReferenceFromUrl(imageUri)
        ref.getBytes(1024*1024*4).addOnSuccessListener{
            val imagePath = imagePath1+"/"+imageUri.split("/$lvl/images/")[1]
            File(imagePath1).mkdirs()
            val wr = FileOutputStream(File(imagePath))
            wr.write(it)
            wr.flush()
            wr.close()
        }

    }

    override fun checkAuthoriztionStatus(): Boolean {
        auth = FirebaseAuth.getInstance()
        val currentUser = auth.currentUser
        return when(currentUser){
            null -> false
            else -> true
        }
    }
}


package ru.polydevochki.startapp.screens.testing

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.QuerySnapshot
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import ru.polydevochki.startapp.App
import ru.polydevochki.startapp.data.AnswerModel
import ru.polydevochki.startapp.data.QuestionModel
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.schedule

class TestRepository : TestContract.Repository {
    private lateinit var mPresenter: TestContract.Presenter
    private lateinit var articlesCount: QuerySnapshot

    override fun attachPresenter(presenter: TestContract.Presenter) {
        mPresenter = presenter
    }

    override fun getQuestions() {
        val sample = ArrayList<QuestionModel>()
        var qqq = ArrayList<AnswerModel>()
        qqq.add(AnswerModel("Да", 3))
        qqq.add(AnswerModel("Нет", 3))
        sample.add(QuestionModel("Есть ли у вас инвесторы?", qqq))
        qqq = ArrayList()
        qqq.add(AnswerModel("В людях", 2))
        qqq.add(AnswerModel("В долларах", 2))
        qqq.add(AnswerModel("В процентах",3))
        sample.add(QuestionModel("В чем измеряется конверсия воронки продаж?", qqq))
        qqq = ArrayList()
        qqq.add(AnswerModel("Да", 3))
        qqq.add(AnswerModel("Нет", 1))
        sample.add(QuestionModel("Определились ли вы с нишей, в которой вы бы хотели развивать свой стартап?", qqq))
        qqq = ArrayList()
        qqq.add(AnswerModel("Минимально жизнеспособный продукт", 3))
        qqq.add(AnswerModel("Самый ценный игрок", 2))
        qqq.add(AnswerModel("Минимальная оплата труда", 2))
        sample.add(QuestionModel("Что такое MVP?", qqq))
        qqq = ArrayList()
        qqq.add(AnswerModel("Реально достижимый объем рынка", 1))
        qqq.add(AnswerModel("Общий объем целевого рынка", 1))
        qqq.add(AnswerModel("Доступный объем рынка", 3))
        sample.add(QuestionModel("Для оценки объема рынка используют анализ через TAM — SAM — SOM. Что означает SAM?", qqq))
        qqq = ArrayList()
        qqq.add(AnswerModel("Development", 1))
        qqq.add(AnswerModel("Data", 3))
        qqq.add(AnswerModel("Demand", 1))
        sample.add(QuestionModel("Что в названии HADI-цикла означает буква D?", qqq))
        qqq = ArrayList()
        qqq.add(AnswerModel("Да", 1))
        qqq.add(AnswerModel("Нет", 3))
        sample.add(QuestionModel("Является ли гипотезой утверждение «если съесть ведро мороженого – замерзнешь»?", qqq))
        mPresenter.onQuestionsLoaded(sample)
    }

    override fun onUserDataGot(name: String, city: String) {
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        db.collection("users").document(userID).update("city",city)
        db.collection("users").document(userID).update("name",name)
    }

    override fun onLevelGot(level: Int){
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        App.setPrefs("lvl","lvl$level")
        if(level == 1){
            db.collection("users").document(userID).update("lvl",level)
        } else if(level == 2){
            db.collection("users").document(userID).update("lvl",level)
            db.collection("users").document(userID).update("readed Articles",3)
        } else if(level == 3){
            db.collection("users").document(userID).update("lvl",level)
            db.collection("users").document(userID).update("readed Articles",6)
        }
        loadCardsDB()
    }

    fun loadCardsDB(){
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        val userDoc = db.collection("users").document(userID).get()
        userDoc.addOnSuccessListener {
            val lvl = it["lvl"].toString()
            val flag = it["lvl${lvl}flag"].toString().toBoolean()
            loadToDB(flag,lvl)
        }
    }

    fun loadToDB(idflag:Boolean, lvl:String){
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        if(!idflag){
            val htasks1 = db.collection("hometasks").document("lvl${lvl}").collection("hometasks").get()
            htasks1.addOnSuccessListener {
                articlesCount = it
                for(doc in it){
                    val card = hashMapOf(
                        "lesson" to doc["lesson"].toString(),
                        "module" to doc["module"].toString(),
                        "status" to doc["status"].toString(),
                        "textExpanded" to doc["textExpanded"].toString(),
                        "time" to doc["time"].toString(),
                        "title" to doc["title"].toString())
                    db.collection("users").document(userID).collection("lvl${lvl}").document(doc["title"].toString()).set(card)
                }
            }
            db.collection("users").document(userID).update("lvl${lvl}flag",true)
        }
    }


}
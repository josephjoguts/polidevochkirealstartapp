package ru.polydevochki.startapp.screens.main.fragments.profile

import android.util.Log
import android.view.View
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.DocumentReference
import com.google.firebase.firestore.Source
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import ru.polydevochki.startapp.App
import java.io.File
import kotlin.math.roundToInt


class ProfileRepository : ProfileContract.Repository {

    private lateinit var mPresenter: ProfileContract.Presenter

    override fun attachPresenter(presenter: ProfileContract.Presenter) {
        mPresenter = presenter
    }

    override fun logOut() {
        val t = File("data/data/${App.instance.packageName}/shared_prefs/Article.xml")
        if (t.exists())
            t.deleteRecursively()
        FirebaseAuth.getInstance().signOut()
    }

    override fun getNames(v: View?) {
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        val ref = db.collection("users").document(userID)
        ref.get().addOnSuccessListener {
            if (it != null) {

                val name = it.get("name").toString()
                val lvl = it.get("lvl").toString()
                val city = it.get("city").toString()
                calculateProgress(ref, lvl)
                App.setPrefs("lvl", "lvl${lvl}")
                App.setPrefs("name", name)
                App.setPrefs("city", city)
                mPresenter.showData(name, lvl, city, v);

            }

        }.addOnCanceledListener {
            val name = App.getPrefs("name", "Anton Edlenko")
            val lvl = App.getPrefs("lvl", "lvl1").slice(0..2)
            val city = App.getPrefs("city", "City-17")
            mPresenter.showData(name, lvl, city, v);
        }
        //       db.collection("users").document(userID).collection("lvl")

    }

    override fun setName(name: String) {
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        db.collection("users").document(userID).update("name", name)
    }

    override fun setCity(city: String) {
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        db.collection("users").document(userID).update("city", city)
    }

    private fun calculateProgress(ref: DocumentReference, lvl: String) {
//        ref.collection("lvl${lvl}").get().addOnSuccessListener {
//            if (it != null) {
//                var count = 0
//                for (document in it.documents) {
//                    if (document["status"]!!.equals("DONE"))
//                        count++
//                }
//                Log.d("HAHA","$count")
//                val proc = (count.toDouble() / it.documents.count().toDouble()) * 100
//                App.setPrefs("progress", proc.roundToInt().toString())
//                mPresenter.showProgress(proc.roundToInt())
//            }
//
//        }.addOnCanceledListener {
//            val proc = App.getPrefs("progress", "0");
//            mPresenter.showProgress(proc!!.toInt())
//        }
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        db.collection("users").document(userID).get().addOnSuccessListener {
            val proc = ((it.get("readed Articles")as Long)/21.0 * 100).roundToInt()
            App.setPrefs("progress", proc.toString())
            mPresenter.showProgress(proc)
        }.addOnFailureListener {
            val proc = App.getPrefs("progress", "0");
            mPresenter.showProgress(proc.toInt())
        }
    }


}
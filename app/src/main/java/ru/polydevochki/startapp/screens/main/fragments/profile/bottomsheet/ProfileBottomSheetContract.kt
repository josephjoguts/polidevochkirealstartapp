package ru.polydevochki.startapp.screens.main.fragments.profile.bottomsheet

import android.graphics.drawable.Drawable
import ru.polydevochki.startapp.data.BotttomSheetDrawableModel
import ru.polydevochki.startapp.screens.main.fragments.hometask.HometaskContract
import ru.polydevochki.startapp.screens.main.fragments.profile.ProfileContract

interface ProfileBottomSheetContract {
    interface Viev{}

    interface Presenter
    {
        fun attachView(view: ru.polydevochki.startapp.screens.main.fragments.profile.bottomsheet.ProfileBottomSheetContract.Viev)
        fun detachView()
        abstract fun loadImages():  Array<BotttomSheetDrawableModel>
    }
    interface Repository
    {
        fun attachPresenter(presenter: Presenter)
        abstract fun loadImages(): Array<BotttomSheetDrawableModel>

    }

}
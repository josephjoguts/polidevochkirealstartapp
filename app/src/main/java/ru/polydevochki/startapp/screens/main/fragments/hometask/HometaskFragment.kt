package ru.polydevochki.startapp.screens.main.fragments.hometask

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.fragment_hometask.*
import kotlinx.android.synthetic.main.fragment_hometask.view.rvHometask
import ru.polydevochki.startapp.R
import ru.polydevochki.startapp.data.HometaskData
import ru.polydevochki.startapp.App
import ru.polydevochki.startapp.screens.main.fragments.hometask.adapters.HometaskAdapter
import kotlin.collections.ArrayList

class HometaskFragment: Fragment(), HometaskContract.View {
    private val mPresenter by lazy { HometaskPresenter() }
    lateinit var hometaskAdapter: HometaskAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        loadAdapter()
        mPresenter.loadInfo()
    }

    private fun loadAdapter() {
        hometaskAdapter = HometaskAdapter(object: HometaskAdapter.OnItemInteractionListener{
            override fun onItemInteraction(item: HometaskData?) {
                Toasty.success(App.instance,"Сделано!",Toast.LENGTH_SHORT).show()
                hometaskAdapter.notifyDataSetChanged()
                mPresenter.changeCond(item!!)
                mPresenter.loadInfo()

                //var arr = hometaskAdapter.getItems()
                //updateHometask(arr)
                }
            }
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_hometask, null)
        v.rvHometask.apply {
            adapter = hometaskAdapter
            hometaskAdapter.rv = v.rvHometask
            layoutManager = LinearLayoutManager(inflater.context)
            setHasFixedSize(true)
        }
        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mPresenter.attachView(this)
    }

    override fun onDetach() {
        super.onDetach()
        mPresenter.detachView()
    }

    override fun updateHometask(items: ArrayList<HometaskData>){
        hometaskAdapter.updateItems(items)
        this.progressTasks.visibility = View.GONE
        this.rvHometask.visibility = View.VISIBLE
    }

}
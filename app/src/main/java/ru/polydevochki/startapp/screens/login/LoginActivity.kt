package ru.polydevochki.startapp.screens.login

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.TypedValue
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import es.dmoral.toasty.Toasty
import kotlinx.android.synthetic.main.activity_login.*
import ru.polydevochki.startapp.R
import ru.polydevochki.startapp.screens.main.MainActivity
import ru.polydevochki.startapp.screens.testing.TestActivity

class LoginActivity : AppCompatActivity(), LoginContract.View {
    private var currentMode: LayoutMode = LayoutMode.LOGIN

    private val mPresenter by lazy {
        LoginPresenter()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setUpListeners()
    }

    override fun onStart() {
        super.onStart()
        mPresenter.attachView(this)
    }

    override fun onStop() {
        super.onStop()
        mPresenter.detachView()
    }

    private fun setUpListeners() {
        btnBack.setOnClickListener {
            onBackPressed()
        }
        btnLogin.setOnClickListener {
            when (currentMode) {
                LayoutMode.LOGIN -> mPresenter.onBtnLoginClick(
                    etLogin.text.toString().trim(),
                    etPassword.text.toString().trim()
                )
                LayoutMode.REGISTRATION -> mPresenter.onBtnCreateAccountClick(
                    etLogin.text.toString().trim(),
                    etPassword.text.toString().trim()
                )
                LayoutMode.RESTORING -> mPresenter.onBtnResetClick(etLogin.text.toString().trim())
            }
        }

        btnCreateAccount.setOnClickListener {
            enterRegistrationMode()
        }

        btnForgot.setOnClickListener {
            enterRestoringMode()
        }

        etPassword.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (text!!.trim().isNotEmpty() && text.trim().length < 6) {
                    showPasswordError("Пароль должен быть больше 5 символов")
                } else {
                    btnLogin.isEnabled = true
                    tilPassword.error = null
                }
            }
        })
        etLogin.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                btnLogin.isEnabled = true
                tilLogin.error = null
            }
        })
    }

    override fun showToast(message: String, mode: ToastMode) {
        when (mode) {
            ToastMode.ERROR -> Toasty.error(
                applicationContext,
                message,
                Toast.LENGTH_SHORT,
                true
            ).show()
            ToastMode.SUCCESS -> Toasty.success(
                applicationContext,
                message,
                Toast.LENGTH_SHORT,
                true
            ).show()
            ToastMode.WARNING -> Toasty.warning(
                applicationContext,
                message,
                Toast.LENGTH_SHORT,
                true
            ).show()
            ToastMode.INFO -> Toasty.info(
                applicationContext,
                message,
                Toast.LENGTH_SHORT,
                true
            ).show()
        }
    }

    override fun launchTestActivity() {
        startActivity(Intent(this, TestActivity::class.java))
        finish()
    }

    override fun launchMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

    override fun showLoginError(text: String) {
        btnLogin.isEnabled = false
        tilLogin.error = text
    }

    override fun showPasswordError(text: String) {
        btnLogin.isEnabled = false
        tilPassword.error = text
    }

    override fun showLoadProgress() {
        pbLoading.visibility = View.VISIBLE
        btnLogin.text = ""
        btnLogin.isEnabled = false
        btnBack.isEnabled = false
    }

    override fun hideLoadProgress() {
        pbLoading.visibility = View.INVISIBLE
        btnLogin.text = when (currentMode) {
            LayoutMode.LOGIN -> resources.getString(R.string.log_in)
            LayoutMode.REGISTRATION -> resources.getString(R.string.create_new_account)
            LayoutMode.RESTORING -> resources.getString(R.string.restore)
        }
        btnLogin.isEnabled = true
        btnBack.isEnabled = true
    }

    override fun enterLoginMode() {
        btnBack.visibility = View.GONE
        btnLogin.text = resources.getString(R.string.log_in)
        btnCreateAccount.visibility = View.VISIBLE
        btnForgot.visibility = View.VISIBLE
        tilPassword.visibility = View.VISIBLE
        currentMode = LayoutMode.LOGIN
    }

    private fun enterRegistrationMode() {
        btnBack.visibility = View.VISIBLE
        btnLogin.text = resources.getString(R.string.create_new_account)
        btnCreateAccount.visibility = View.GONE
        btnForgot.visibility = View.GONE
        tilPassword.visibility = View.VISIBLE
        currentMode = LayoutMode.REGISTRATION
    }

    private fun enterRestoringMode() {
        btnBack.visibility = View.VISIBLE
        btnLogin.text = resources.getString(R.string.restore)
        btnCreateAccount.visibility = View.GONE
        btnForgot.visibility = View.GONE
        tilPassword.visibility = View.GONE
        currentMode = LayoutMode.RESTORING
    }

    override fun onBackPressed() {
        if (currentMode == LayoutMode.LOGIN) {
            finish()
        } else {
            enterLoginMode()
        }
    }

    companion object {
        enum class LayoutMode { LOGIN, REGISTRATION, RESTORING }
        enum class ToastMode { ERROR, SUCCESS, WARNING, INFO }
    }
}

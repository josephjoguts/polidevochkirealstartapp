package ru.polydevochki.startapp.screens.main.fragments.profile.bottomsheet.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_bottomsheetimage.view.*
import ru.polydevochki.startapp.App
import ru.polydevochki.startapp.R
import ru.polydevochki.startapp.data.BotttomSheetDrawableModel
import ru.polydevochki.startapp.screens.main.fragments.profile.fromBottomSheet

class ProfileBottomSheetAdapter (private val dataSet:Array<BotttomSheetDrawableModel>, fromBottomSheet:fromBottomSheet):RecyclerView.Adapter<ProfileBottomSheetAdapter.ViewHolder>() {
    private val fromBottomSheet: fromBottomSheet
    init {
        this.fromBottomSheet = fromBottomSheet
    }
    inner class ViewHolder(v:View):RecyclerView.ViewHolder(v)
    {
        var ImageSrc:ImageView
        init
        {
            ImageSrc =v.bottomsheetimage
        }
    }
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ):ViewHolder {
            val v = LayoutInflater.from(parent.context).inflate(R.layout.item_bottomsheetimage,null)
            return ViewHolder(v)

    }

    override fun getItemCount() = dataSet.size

    override fun onBindViewHolder(holder: ProfileBottomSheetAdapter.ViewHolder, position: Int) {
        holder.ImageSrc.setImageDrawable(dataSet[position].d)
        holder.ImageSrc.setOnClickListener{
            fromBottomSheet.showImage(dataSet[position].d)
            App.setPrefs("avatar",dataSet[position].path)
        }


    }

}
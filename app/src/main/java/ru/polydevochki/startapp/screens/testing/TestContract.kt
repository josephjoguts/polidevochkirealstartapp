package ru.polydevochki.startapp.screens.testing

import ru.polydevochki.startapp.data.QuestionModel

interface TestContract {
    interface View{
        fun initTest(questions: ArrayList<QuestionModel>)
        fun showNameError(text: String)
        fun showCityError(text: String)
        fun startTest()
        fun showSkipAcceptDialog()
        fun launchMainActivity()
    }

    interface Presenter{
        fun attachView(view: View)
        fun detachView()
        fun onQuestionsLoaded(data: ArrayList<QuestionModel>)
        fun onBtnSendClicked(name: String, city: String)
        fun onBtnEndClick(level: Int)
        fun getQuestions()
    }

    interface Repository{
        fun attachPresenter(presenter: Presenter)
        fun getQuestions()
        fun onUserDataGot(name: String, city: String)
        fun onLevelGot(level: Int)
    }
}
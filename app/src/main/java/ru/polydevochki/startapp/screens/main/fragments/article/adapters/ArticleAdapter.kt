package ru.polydevochki.startapp.screens.main.fragments.article.adapters

import android.content.Intent
import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatImageView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_article.view.*
import ru.polydevochki.startapp.R
import ru.polydevochki.startapp.data.ArticleCard
import ru.polydevochki.startapp.screens.readinarticle.ReadinArticleActivity


class ArticleAdapter(private val dataSet: Array<ArticleCard>) :
    RecyclerView.Adapter<ArticleAdapter.ViewHolder>() {
    inner class ViewHolder(v: View) : RecyclerView.ViewHolder(v)
    {
        var ImageSrc :ImageView;
        val ArticleName:TextView;
        val ReadingTime:TextView;
        val CheckBox:AppCompatImageView;
        val Module:TextView
        init
        {
            ImageSrc = v.articleImage
            ArticleName = v.articlename
            ReadingTime = v.articlereadingtime
            CheckBox = v.articleCheckBox
            Module =v.tvModule
        }
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context).inflate(R.layout.item_article,null);
        v.setOnClickListener{
            val articlePath = v.articlename.tag.toString();
            val intent = Intent(viewGroup.context,ReadinArticleActivity::class.java);
            intent.putExtra("ARTICLE_PATH",articlePath);
            intent.putExtra("ARTICLE_NAME",v.articlename.text)
            intent.putExtra("ARTICLE_LVL",v.tvModule.text)
            viewGroup.context.startActivity(intent)

        }
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
       // viewHolder.ImageSrc.setBackgroundColor(Color.LTGRAY)
        //viewHolder.ImageSrc.drawable = dataSet[position].imageSrc

      //  Picasso.Builder(MyApp.instance).build().load(dataSet[position].imagePath).into(viewHolder.ImageSrc)
        val bitmap = BitmapFactory.decodeFile(dataSet[position].imagePath);
        viewHolder.ImageSrc.setImageBitmap(bitmap)
        viewHolder.ArticleName.text = dataSet[position].name
        viewHolder.ArticleName.tag = dataSet[position].path
        viewHolder.ReadingTime.text = dataSet[position].readProgress
        viewHolder.Module.text = dataSet[position].Module
        when(dataSet[position].check)
        {
            false ->{
                viewHolder.ReadingTime.text = dataSet[position].readProgress
                viewHolder.CheckBox.setImageResource(R.drawable.ic_time_24dp)
            }

            true ->
            {
                viewHolder.ReadingTime.text = "Прочитано"
                viewHolder.CheckBox.setImageResource(R.drawable.ic_check_black_24dp)

            }

        }

        //var tint = if (dataSet[position].check) R.color.orangeArticleYes else R.color.articleChechBox

       //viewHolder.check.setColorFilter(tint)
    }

    override fun getItemCount() = dataSet.size
}
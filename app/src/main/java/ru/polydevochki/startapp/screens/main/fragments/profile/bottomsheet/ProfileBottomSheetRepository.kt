package ru.polydevochki.startapp.screens.main.fragments.profile.bottomsheet

import android.graphics.drawable.Drawable
import android.util.Log
import ru.polydevochki.startapp.App
import ru.polydevochki.startapp.data.BotttomSheetDrawableModel

class ProfileBottomSheetRepository:ProfileBottomSheetContract.Repository{
    private lateinit var mPresenter: ProfileBottomSheetContract.Presenter
    override fun attachPresenter(presenter: ProfileBottomSheetContract.Presenter) {
        mPresenter = presenter
    }

    override fun loadImages(): Array<BotttomSheetDrawableModel> {
        val context = App.instance
        val assets = context.assets
        var arr = mutableListOf<BotttomSheetDrawableModel>()

        for(x in assets.list("images/test")!!)
        {

        val inpStream = assets.open("images/test/$x")
            Log.d("IMAGES","images/test/$x")
        val d = Drawable.createFromStream(inpStream,null)
          arr.add(BotttomSheetDrawableModel(d,"images/test/$x"))
        }
       return arr.toTypedArray();
    }

}
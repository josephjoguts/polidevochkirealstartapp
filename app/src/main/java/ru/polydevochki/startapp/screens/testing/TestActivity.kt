package ru.polydevochki.startapp.screens.testing

import android.animation.LayoutTransition
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_test.*
import kotlinx.android.synthetic.main.activity_test.rvChatHistory
import kotlinx.android.synthetic.main.activity_test.view.*
import ru.polydevochki.startapp.R
import ru.polydevochki.startapp.data.AnswerModel
import ru.polydevochki.startapp.data.QuestionModel
import ru.polydevochki.startapp.screens.main.MainActivity
import ru.polydevochki.startapp.screens.testing.adapters.AnswersAdapter
import ru.polydevochki.startapp.screens.testing.adapters.ChatHistoryAdapter


class TestActivity : AppCompatActivity(), TestContract.View {
    private lateinit var chatHistoryAdapter: ChatHistoryAdapter
    private lateinit var chatHistoryLayoutManager: LinearLayoutManager
    private lateinit var answersAdapter: AnswersAdapter
    private var currentLevel = 3

    private val mPresenter: TestContract.Presenter by lazy {
        TestPresenter()
    }

    override fun onStart() {
        super.onStart()
        mPresenter.attachView(this)
        if(answersAdapter.getItemCount() == 0){
            mPresenter.getQuestions()
        }
    }

    override fun onStop() {
        super.onStop()
        mPresenter.detachView()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        setupUI()
    }

    private fun setupUI(){
        setupAdapters()
        setupRecyclerViews()
        setupListeners()
    }

    private fun setupAdapters() {
        chatHistoryAdapter = ChatHistoryAdapter()
        answersAdapter = AnswersAdapter(llQuestions, object : AnswersAdapter.AnswerCallBack{
            override fun onAnswerSelected(answer: AnswerModel) {
                if (answer.impact < currentLevel){
                    currentLevel = answer.impact
                }
                chatHistoryAdapter.addItem(answer.text,false)
                nextQuestion()
            }
        })
        chatHistoryLayoutManager = LinearLayoutManager(applicationContext).apply {
            stackFromEnd = true
        }
    }

    private fun setupRecyclerViews() {
        rvChatHistory.apply {
            layoutManager = chatHistoryLayoutManager
            adapter = chatHistoryAdapter
        }
        chatHistoryAdapter.registerAdapterDataObserver(object: RecyclerView.AdapterDataObserver() {
            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                super.onItemRangeInserted(positionStart, itemCount)
                rvChatHistory.smoothScrollToPosition(chatHistoryAdapter.itemCount)
            }
        })
    }

    private fun setupListeners() {
        btnSend.setOnClickListener {
            hideSoftKeyboard(llTextInput)
            mPresenter.onBtnSendClicked(etName.text.toString().trim(),etCity.text.toString().trim())
        }
        btnEnd.setOnClickListener {
            mPresenter.onBtnEndClick(currentLevel)
        }

        btnSkip.setOnClickListener {
            onBackPressed()
        }
        etName.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (text!!.trim().isEmpty()) {
                    showNameError(resources.getString(R.string.edit_is_empty))
                } else {
                    btnSend.isEnabled = true
                    tilName.error = null
                }
            }
        })
        etCity.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(text: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (text!!.trim().isEmpty()) {
                    showCityError(resources.getString(R.string.edit_is_empty))
                } else {
                    btnSend.isEnabled = true
                    tilCity.error = null
                }
            }
        })
    }

    override fun onBackPressed() {
        showSkipAcceptDialog()
    }

    override fun startTest(){
        chatHistoryAdapter.addItem("""Меня зовут ${etName.text.toString().trim()}, я из г. ${etCity.text.toString().trim()}""",false)
        llTextInput.visibility = View.GONE
        chatHistoryAdapter.addItem(resources.getString(R.string.intro_second),true)
        nextQuestion()
    }

    private fun nextQuestion(){
        pbStatus.progress = answersAdapter.getCurrentQuestionNumber() + 1
        tvStatus.text = (answersAdapter.getCurrentQuestionNumber() + 1).toString()
        val question =  answersAdapter.nextQuestion()
        if(question == ""){
            option1.visibility = View.GONE
            option2.visibility = View.GONE
            option3.visibility = View.GONE
            flEndContainer.visibility = View.VISIBLE
            chatHistoryAdapter.addItem(resources.getString(R.string.intro_end),true)
            tvYourLevel.text = resources.getString(R.string.your_level_is, currentLevel.toString())
            return
        }
        chatHistoryAdapter.addItem(question,true)
    }

    override fun showNameError(text: String) {
        btnSend.isEnabled = false
        tilName.error = text
    }

    override fun showCityError(text: String) {
        btnSend.isEnabled = false
        tilCity.error = text
    }

    override fun initTest(questions: ArrayList<QuestionModel>){
        answersAdapter.updateQuestions(questions)
        chatHistoryAdapter.addItem(resources.getString(R.string.intro_start),true)
    }

    private fun hideSoftKeyboard(view: View) {
        val imm: InputMethodManager =
            getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    override fun showSkipAcceptDialog(){
        val dialog = AlertDialog.Builder(this)
        dialog.setCancelable(true)
        dialog.setPositiveButton(getString(R.string.continuee)) { _, _ ->
            mPresenter.onBtnEndClick(1)
        }
        dialog.setNegativeButton(getString(R.string.cancel)) { dialogInterface, i ->
            dialogInterface.cancel()
        }
        dialog.setTitle(getString(R.string.skip_test))
        dialog.setMessage(getString(R.string.skip_test_exp))
        dialog.show()
    }

    override fun launchMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }

}

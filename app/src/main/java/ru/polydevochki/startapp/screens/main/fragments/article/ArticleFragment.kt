package ru.polydevochki.startapp.screens.main.fragments.article

import android.content.Context
import android.opengl.Visibility
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.fragment_article.*
import kotlinx.android.synthetic.main.fragment_article.view.*
import kotlinx.android.synthetic.main.fragment_article.view.articleProgressBar
import ru.polydevochki.startapp.R
import ru.polydevochki.startapp.data.ArticleCard
import ru.polydevochki.startapp.App
import ru.polydevochki.startapp.screens.main.fragments.article.adapters.ArticleAdapter
import java.io.File


class ArticleFragment : Fragment(), ArticleContract.View {

    lateinit var v: View
    private val mPresenter by lazy { ArticlePresenter() }
    private lateinit var listenerRegistration: ListenerRegistration
    lateinit var articlesAdapter: ArticleAdapter;
    val DATA_HAS_CHANGED = 1;


    private fun loadAdapter() {
        mPresenter.loadInfo(this.context, 0)
    }

    override fun onStart() {
        super.onStart()
        loadAdapter()
        loadDatabase()
    }

    private fun loadDatabase() {
        val db = Firebase.firestore
        val context = App.instance
        val lvl = context.getSharedPreferences("Articles",0).getString("lvl","lvl1");
        val otherlvl = lvl!!.split("vl")[1].toInt()
        var upd = context.getSharedPreferences("Articles",0).getString("databaseUpd$lvl","0");
        //Log.d("upd",upd)
        for(i in 1..otherlvl) {
           db.collection("articles").document("lvl$i")
                .addSnapshotListener { snapshot, e ->
                    if (snapshot != null && snapshot.exists() && !upd.equals(snapshot["upd"].toString())) {
                        val newupd = snapshot["upd"].toString();
                        upd = context.getSharedPreferences("Articles", 0)
                            .getString("databaseUpdlvl$i", "0");
                        if(!newupd.equals(upd)) {
                            context.getSharedPreferences("Articles", 0).edit()
                                .putString("databaseUpdlvl$i", snapshot["upd"].toString()).apply()
                            Log.d("upd", i.toString())
                            mPresenter.loadInfo(context, i);
                        }
                    }
                }
        }


    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val context = App.instance

        val path1 = "/data/data/" + context.packageName + "/articles/libs"
        var file = File(path1)
        Log.d("i exist", "${file.exists()}")


        v = inflater.inflate(R.layout.fragment_article, null);
        v.rvArticles.apply {
            layoutManager = LinearLayoutManager(inflater.context)
            setHasFixedSize(true)
        }
        return v
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mPresenter.attachView(this);
    }

    override fun onPause() {
        super.onPause()

    }

    override fun onStop() {
        super.onStop()
//        listenerRegistration.remove()
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onDetach() {
        super.onDetach()
        mPresenter.detachView()
    }

    override fun showRecycler(
        arr: Array<ArticleCard>,
        articleCount: Int
    ) {
        try {
            articleProgressBar.visibility = View.GONE;

            // arr.sortWith(compareBy { it.Module })
            articlesAdapter = ArticleAdapter(arr)
            v.rvArticles.adapter = articlesAdapter;
        }catch (e:Exception){}
    }
}
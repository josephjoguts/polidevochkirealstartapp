package ru.polydevochki.startapp.screens.main.fragments.hometask

import ru.polydevochki.startapp.data.HometaskData


class HometaskPresenter : HometaskContract.Presenter {

    var viewIsAttached = false
    var mView: HometaskContract.View? = null

    private val mRepository: HometaskContract.Repository by lazy {
        HometaskRepository()
    }

    override fun attachView(view: HometaskContract.View) {
        viewIsAttached = true
        mView = view
        mRepository.attachPresenter(this)
    }

    override fun loadInfo() {
        mRepository.loadCardsDB()
    }


    override fun detachView() {
        viewIsAttached = false
        mView = null
    }

    override fun onHomeTaskLoaded(items: ArrayList<HometaskData>) {
        if (viewIsAttached) {
            mView!!.updateHometask(items)
        }
    }

    override fun changeCond(item: HometaskData) {
        if (viewIsAttached) {
            val name = item.title
            mRepository.changeCondition(name)
        }
    }

}
package ru.polydevochki.startapp.screens.main.fragments.article

import android.content.Context
import ru.polydevochki.startapp.data.ArticleCard

interface ArticleContract {

    interface View{
        fun showRecycler(
            arr: Array<ArticleCard>,
            articleCount: Int
        );

    }

    interface Presenter {
        fun attachView(view: View)
        fun detachView()
        fun loadInfo(context: Context?, changeType: Int)
        fun loadAdapter(
            arr: Array<ArticleCard>,
            articleCount: Int
        );

    }

    interface Repository {
        fun attachPresenter(presenter: Presenter)
        fun loadInfo(context: Context?, changeType: Int)
    }
}
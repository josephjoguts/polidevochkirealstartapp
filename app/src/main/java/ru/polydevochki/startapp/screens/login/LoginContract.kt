package ru.polydevochki.startapp.screens.login


interface LoginContract {
    interface View {
        fun launchTestActivity()
        fun launchMainActivity()
        fun showLoginError(text: String)
        fun showPasswordError(text: String)
        fun showLoadProgress()
        fun hideLoadProgress()
        fun enterLoginMode()
        fun showToast(message: String, mode: LoginActivity.Companion.ToastMode)
    }

    interface Presenter {
        fun attachView(view: View)
        fun detachView()
        fun onBtnLoginClick(loginText: String, passText: String)
        fun onBtnResetClick(loginText: String)
        fun onBtnCreateAccountClick(login: String, password: String)
        fun onLoginFinished()
        fun onRegistrationFinished()
        fun onResetFinished()
        fun onResetFailed(reason: String)
        fun onRegistrationFailed(reason: String)
        fun onLoginFailed(reason: String)
    }

    interface Repository {
        fun resetPassword(email: String)
        fun checkAccount(): Boolean
        fun attachPresenter(presenter: Presenter)
        fun createAccount(email: String, password: String)
        fun signInAccount(email: String, password: String)
        fun createUserDocument()
    }

}
package ru.polydevochki.startapp.utils

import android.content.Context
import android.util.Log
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import es.dmoral.toasty.Toasty
import ru.polydevochki.startapp.App
import ru.polydevochki.startapp.R
import java.io.IOException
import java.util.*
import kotlin.math.abs

object Utils {

    fun countTakenTime(time: Calendar): String {
        val days =
            abs(Calendar.getInstance().get(Calendar.DAY_OF_MONTH) - time.get(Calendar.DAY_OF_MONTH))
        val month = abs(Calendar.getInstance().get(Calendar.MONTH) - time.get(Calendar.MONTH))
        val hours = abs(Calendar.getInstance().get(Calendar.HOUR) - time.get(Calendar.HOUR))
        val millisMinutes = abs(Calendar.getInstance().timeInMillis - time.timeInMillis)
        val minutes = java.util.concurrent.TimeUnit.MILLISECONDS.toMinutes(millisMinutes).toInt()
        if (month >= 1) return "больше месяца назад"
        else {
            if (days > 1) {
                return when (days) {
                    1, 21, 31 -> "$days день назад"
                    2, 3, 4, 22, 23, 24 -> "$days дня назад"
                    else -> "$days дней назад"
                }
            } else {
                if (hours > 1) {
                    return when (hours) {
                        1, 21 -> "$hours час назад"
                        2, 3, 4, 22, 23, 24 -> "$hours часа назад"
                        else -> "$hours часов назад"
                    }
                } else {
                    if (minutes > 1) {
                        return when (minutes) {
                            1, 21, 31, 41, 51 -> "$minutes минута назад"
                            2, 3, 4, 22, 23, 24, 32, 33, 34, 42, 43, 44, 52, 53, 54 -> "$minutes минуты назад"
                            else -> "$minutes минут назад"
                        }
                    } else return "меньше минуты назад"
                }
            }
        }
    }

    fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }

    fun getErrorMessage(errorID: String): String {
        Log.d("DEBUG_ERROR", errorID)
        return when (errorID) {
            "ERROR_WRONG_PASSWORD" -> App.instance.getString(R.string.error_password_incorrect)
            "ERROR_ACCOUNT_EXISTS_WITH_DIFFERENT_CREDENTIAL" -> App.instance.getString(R.string.error_different_credential)
            "ERROR_EMAIL_ALREADY_IN_USE" -> App.instance.getString(R.string.error_email_is_used)
            "ERROR_CREDENTIAL_ALREADY_IN_USE" -> App.instance.getString(R.string.error_credential_is_used)
            "ERROR_USER_DISABLED" -> App.instance.getString(R.string.error_user_banned)
            "ERROR_USER_NOT_FOUND" -> App.instance.getString(R.string.error_user_not_found)
            "ERROR_WEAK_PASSWORD" -> App.instance.getString(R.string.error_weak_password)
            "ERROR_INVALID_EMAIL" -> App.instance.getString(R.string.error_invalid_email)
            "ERROR_NETWORK" -> App.instance.getString(R.string.error_network)
            else -> App.instance.getString(R.string.error_unknown_error)
        }
    }

    fun changeLevel(cnt: Int){
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        val userDoc = db.collection("users").document(userID).get()
        changeDB(cnt)
    }

    fun changeDB(cnt : Int){
        val db = Firebase.firestore
        val userID = FirebaseAuth.getInstance().currentUser!!.email!!
        if (cnt == 3){
            db.collection("users").document(userID).update("lvl",2)
            App.setPrefs("lvl","lvl2")
            Toasty.success(App.instance,"Вы перешли на 2 уровень!",Toasty.LENGTH_SHORT).show()
        }
        else if (cnt == 6) {
            db.collection("users").document(userID).update("lvl",3)
            App.setPrefs("lvl","lvl3")
            Toasty.success(App.instance,"Вы перешли на 3 уровень!",Toasty.LENGTH_SHORT).show()
        }
        else if (cnt == 11) {
            db.collection("users").document(userID).update("lvl",4)
            App.setPrefs("lvl","lvl4")
            Toasty.success(App.instance,"Вы перешли на 4 уровень!",Toasty.LENGTH_SHORT).show()
        }
        else if (cnt ==16){
            db.collection("users").document(userID).update("lvl",5)
            App.setPrefs("lvl","lvl5")
            Toasty.success(App.instance,"Вы перешли на последний 5 уровень!",Toasty.LENGTH_SHORT).show()
        }
    }
}
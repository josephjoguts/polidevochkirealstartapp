package ru.polydevochki.startapp.data

class QuestionModel(
    val text: String,
    val answers: ArrayList<AnswerModel>
) {
}
package ru.polydevochki.startapp.data

enum class Condition {
    LOCKED, IN_WORK, DONE
}
package ru.polydevochki.startapp.data

import java.util.*


data class HometaskData(val module: Int,
                        val lesson: Int,
                        val title: String,
                        var time: Calendar,
                        val textExpanded: String,
                        var status: Condition
){

}
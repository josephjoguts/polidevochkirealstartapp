package ru.polydevochki.startapp.data

import android.graphics.drawable.Drawable

data class ArticleCard(val imagePath:String, val name:String, val readProgress:String, val check:Boolean, val Module:String, val path:String ) {
}
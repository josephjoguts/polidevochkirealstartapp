package ru.polydevochki.startapp.data

class AnswerModel(
    val text: String,
    val impact: Int
) {
}
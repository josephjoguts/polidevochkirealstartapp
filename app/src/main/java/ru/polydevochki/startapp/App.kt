package ru.polydevochki.startapp

import android.app.Application


class App : Application() {
    override fun onCreate() {
        super.onCreate()
        instance = this
    }

    companion object {
        lateinit var instance: App
            private set
        fun setPrefs(key:String,put:String)
        {
            instance.getSharedPreferences("Articles",0).edit().putString(key,put).apply()
        }
        fun getPrefs(key:String,default:String):String
        {
            return instance.getSharedPreferences("Articles",0).getString(key,default).toString()
        }

    }
}